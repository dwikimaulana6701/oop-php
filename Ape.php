<?php

require ('animal.php');

class Ape extends Animal{
    public $name = "Kera Sakti";
    public $legs = 2;
    public $yell = "Auooo";

    public function set_yell($yell){
        $this -> yell = $yell;
    }

    public function get_yell(){
        return $this -> yell;
    }
}
?>