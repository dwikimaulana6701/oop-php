<?php

class Frog extends Animal{
    public $name = "Buduk";
    public $jump = "Hop Hop";

    public function set_jump($jump){
        $this -> jump = $jump;
    }

    public function get_jump(){
        return $this -> jump;
    }
}

?>